package test;

import java.util.Scanner;

public class AddonTest {

	public static void main(String[] args) {
		//System.out.println("Neuer Bestand: " + a1.kaufen(2));
		//System.out.println("Neuer Bestand: " + a2.kaufen(8));
		//System.out.println("Neuer Bestand: " + a1.verbrauchen(3));

		Scanner myScanner = new Scanner(System.in);
		int swValue;

		System.out.println("============================");
		System.out.println("|   MENU SELECTION DEMO    |");
		System.out.println("============================");
		System.out.println("| Options:                 |");
		System.out.println("|        1. Verbrauchen    |");
		System.out.println("|        2. Kaufen         |");
		System.out.println("|        3. Exit           |");
		System.out.println("============================");
		System.out.print(" Select option: ");
		swValue = myScanner.next().charAt(0);

		switch (swValue) {
		case '1':
			verbrauch();
			break;
		case '2':
			kauf();
			break;
		case '3':
			System.exit(1);
			break;
		}
	}

	public static void verbrauch() {
		Addon a1 = new Addon(1, "Futter", 12.0, 2, 7);
		Addon a2 = new Addon(2, "Wasser", 8.0, 3, 10);

		Scanner myScanner = new Scanner(System.in);
		int v, a;

		System.out.println("============================");
		System.out.println("|   MENU SELECTION DEMO    |");
		System.out.println("============================");
		System.out.println("| Options:                 |");
		System.out.println("|        1. Wasser         |");
		System.out.println("|        2. Futter         |");
		System.out.println("|        3. Exit           |");
		System.out.println("============================");
		System.out.print(" Select option: ");
		v = myScanner.next().charAt(0);
		System.out.println("Wie viel wollen Sie verbrauchen?");
		a = myScanner.nextInt();

		switch (v) {
		case '1':
			System.out.println("Neuer Bestand: " + a1.verbrauchen(a));
			break;
		case '2':
			System.out.println("Neuer Bestand: " + a2.verbrauchen(a));
			break;
		case '3':
			System.exit(1);
			break;
		}
	}

	public static void kauf() {
		Addon a1 = new Addon(1, "Futter", 12.0, 2, 7);
		Addon a2 = new Addon(2, "Wasser", 8.0, 3, 10);

		Scanner myScanner = new Scanner(System.in);
		int k, s;

		System.out.println("============================");
		System.out.println("|   MENU SELECTION DEMO    |");
		System.out.println("============================");
		System.out.println("| Options:                 |");
		System.out.println("|        1. Wasser         |");
		System.out.println("|        2. Futter         |");
		System.out.println("|        3. Exit           |");
		System.out.println("============================");
		System.out.print(" Select option: ");
		k = myScanner.next().charAt(0);
		System.out.println("Wie viel wollen Sie verbrauchen?");
		s = myScanner.nextInt();

		switch (k) {
		case '1':
			System.out.println("Neuer Bestand: " + a1.kaufen(s));
			break;
		case '2':
			System.out.println("Neuer Bestand: " + a2.kaufen(s));
			break;
		case '3':
			System.exit(1);
			break;
		}
	}
}