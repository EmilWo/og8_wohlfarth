package test;

public class Addon {

	private int id;
	private String bezeichnung;
	private double preis;
	private int bestand;
	private int maxBestand;

	public Addon(int id, String bezeichnung, double preis, int bestand, int maxBestand) {
		this.id = id;
		this.bezeichnung = bezeichnung;
		this.preis = preis;
		this.bestand = bestand;
		this.maxBestand = maxBestand;
	}

	public Addon() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getBestand() {
		return bestand;
	}

	public void setBestand(int bestand) {
		this.bestand = bestand;
	}

	public int getMaxBestand() {
		return maxBestand;
	}

	public void setMaxBestand(int maxBestand) {
		this.maxBestand = maxBestand;
	}

	public int verbrauchen(int bestand) {
		if (this.bestand - bestand < 0) {
			System.out.println("Kein Verbrauch m�glich!");
		} else {
			this.bestand = this.bestand - bestand;
		}
		return this.bestand;
	}

	public int kaufen(int bestand) {
		if (this.bestand <= 0) {
			this.bestand = 0;
			System.out.println("Bestand leer!");
		} else if (this.bestand + bestand > this.maxBestand) {
			System.out.println("Kein Kauf m�glich!");
		} else if (this.bestand + bestand < this.maxBestand){
			this.bestand = this.bestand + bestand;
		}
		return this.bestand;
	}

	public double gesamtwert() {
		return 0;
	}
}