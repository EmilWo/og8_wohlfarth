package bias;

/**
 * FahrerAutoTest.java
 * Einfaches Beispiel zu einer bidirektionalen 1:1-Assoziation.
 * "Eine Fahrer faehrt ein Auto"
 * @version 2.0 vom 22.12.2012
 * @author Ansorge, OSZ IMT
 */
import java.util.Scanner;

public class FahrerAutoTest {

	private static void anzeigen(Fahrer f) {
		String kennzeichen = "LEER";
		String name = "LEER";
		if (f != null)
			name = f.getName();
		if (f.getAuto() != null)
			kennzeichen = f.getAuto().getKennzeichen();
		System.out.println(" " + name + "\t\t-----> " + kennzeichen);
	}

	private static void anzeigen(Auto a) {
		String kennzeichen = "LEER";
		String name = "LEER";
		if (a != null)
			kennzeichen = a.getKennzeichen();
		if (a.getFahrer() != null)
			name = a.getFahrer().getName();
		System.out.println(" " + kennzeichen + "\t-----> " + name);
	}

	private static void anzeigen(Fahrer f1, Fahrer f2, Auto a1, Auto a2) {
		anzeigen(f1);
		anzeigen(f2);
		anzeigen(a1);
		anzeigen(a2);
		System.out.println();
	}

	private static void menue() {
		System.out.println("\n  Fahrer mit Auto verbinden (1) ");
		System.out.println("  Auto mit Fahrer verbinden (2)");
		System.out.println("  anzeigen (8)      beenden (9)");
	}

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		char nochmals = 'j';
		int auswahl;
		int fahrerAuswahl, autoAuswahl;
		Fahrer f1 = new Fahrer("Anton");
		Fahrer f2 = new Fahrer("Berta");
		Auto a1 = new Auto("A-AA 1111");
		Auto a2 = new Auto("B-BB 2222");
		System.out.println("\tEine Fahrer faehrt ein Auto");
		do {
			menue();
			System.out.print("\t\t\tAuswahl: ");
			auswahl = myScanner.nextInt();
			switch (auswahl) {
			case 1:
				System.out.print("Fahrer auswaehlen <" + f1.getName() + " (1) " + f2.getName() + " (2)>: ");
				fahrerAuswahl = myScanner.nextInt();
				System.out.print("Auto auswaehlen <" + a1.getKennzeichen() + " (1) " + a2.getKennzeichen() + " (2)>: ");
				autoAuswahl = myScanner.nextInt();
				if (fahrerAuswahl == 1) {
					if (autoAuswahl == 1) {
						f1.setAuto(a1);
					} else {
						f1.setAuto(a2);
					}
				} else {
					if (autoAuswahl == 1) {
						f2.setAuto(a1);
					} else {
						f2.setAuto(a2);
					}
				}
				break;
			case 2:
				System.out.print("Auto auswaehlen <" + a1.getKennzeichen() + " (1) " + a2.getKennzeichen() + " (2)>: ");
				autoAuswahl = myScanner.nextInt();
				System.out.print("Fahrer auswaehlen <" + f1.getName() + " (1) " + f2.getName() + " (2)>: ");
				fahrerAuswahl = myScanner.nextInt();
				if (autoAuswahl == 1) {
					if (fahrerAuswahl == 1) {
						a1.setFahrer(f1);
					} else {
						a1.setFahrer(f2);
					}
				} else {
					if (fahrerAuswahl == 1) {
						a2.setFahrer(f1);
					} else {
						a2.setFahrer(f2);
					}
				}
				break;
			case 8:
				anzeigen(f1, f2, a1, a2);
				break;
			case 9:
				nochmals = 'n';
				break;
			default:
			} // end of switch
		} while (nochmals == 'j');

	} // main
}// FahrerAutoTest
