package bias;

public class Fahrer {
	private String name;
	Auto auto;

	public Fahrer(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void unlinkAuto() {

	}

	public Auto getAuto() {
		return auto;
	}

	public void setAuto(Auto auto) {
		this.auto = auto;
	}

	public void removeAuto() {

	}

}
