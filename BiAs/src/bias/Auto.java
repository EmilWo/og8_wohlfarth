package bias;

public class Auto {
	private String kennzeichen;
	Fahrer fahrer;

	public Auto(String kennzeichen) {
		super();
		this.kennzeichen = kennzeichen;
	}

	public String getKennzeichen() {
		return kennzeichen;
	}

	public void setKennzeichen(String kennzeichen) {
		this.kennzeichen = kennzeichen;
	}

	public void unlinkFahrer() {

	}

	public Fahrer getFahrer() {
		return fahrer;
	}

	public void setFahrer(Fahrer fahrer) {
		this.fahrer = fahrer;
	}

	public void removeAuto() {

	}
}
