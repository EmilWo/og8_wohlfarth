package FileManager;

import java.io.*;

public class FileManager {

	private File file;

	public FileManager(File file) {
		this.file = file;
	}

	public void lesen() {
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while ((s = br.readLine()) != null) {
				System.out.println(s);
			}
		} catch (Exception e) {
			System.out.println("File wahrscheinlich nicht vohanden");
			e.printStackTrace();
		}

	}

	public void schreibe(String s) {
		try {
			FileWriter fw = new FileWriter(this.file, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.newLine();
			bw.write(s);
			bw.flush();
		} catch (IOException e) {
			System.out.println("File wahrscheinlich nicht vohanden");
			e.printStackTrace();
		}
	}
}