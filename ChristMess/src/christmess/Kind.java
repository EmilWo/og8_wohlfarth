package christmess;

import java.util.*;

public class Kind {
	// Jedes Kind hat einen Nachnamen, Vornamen, Geburtstag, Wohnort und
	// Bravheitsgrad
	private String vorname;
	private String nachname;
	private String geburtsdatum;
	private String bravheit;
	private String wohnort;

	// Erstellen Sie einen vollparametrisierten Konstruktor, Getter/Setter und eine
	// toString-Methode
	public Kind(String vorname, String nachname, String geburtsdatum, String bravheit, String wohnort) {
		super();
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsdatum = geburtsdatum;
		this.bravheit = bravheit;
		this.wohnort = wohnort;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getGeburtsdatum() {
		return geburtsdatum;
	}

	public void setGeburtsdatum(String geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}

	public String getBravheit() {
		return bravheit;
	}

	public void setBravheit(String bravheit) {
		this.bravheit = bravheit;
	}

	public String getWohnort() {
		return wohnort;
	}

	public void setWohnort(String wohnort) {
		this.wohnort = wohnort;
	}

	@Override
	public String toString() {
		return "Kind [vorname=" + vorname + ", nachname=" + nachname + ", geburtsdatum=" + geburtsdatum + ", bravheit="
				+ bravheit + ", wohnort=" + wohnort + "]";
	}

}
