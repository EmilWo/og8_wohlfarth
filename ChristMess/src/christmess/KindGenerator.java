package christmess;

import java.io.*;
import java.text.*;
import java.util.*;

public class KindGenerator {

	private Kind[] kinder = new Kind[10_000];
	int versuch = 0;

	// Hier eine Methode erstellen, um die Datei der Kinder auszulesen und mit den
	// Daten eine Liste an Kind-Objekte anlegen
	public Kind[] generateKinderListe() {

		File file = new File("kinddaten.txt");
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while ((s = br.readLine()) != null) {
				s = s.replaceAll(",", "");
				String[] kinddaten = s.split(" ");
				String wohnort = "";
				for (int i = 4; i < kinddaten.length; i++) {
					wohnort = wohnort + kinddaten[i];
				}
				Kind k = new Kind(kinddaten[0], kinddaten[1], kinddaten[2], kinddaten[3], wohnort);
				do {
					for (int i = 0; i < this.kinder.length; i++) {
						if (this.kinder[i] == null) {
							this.kinder[i] = k;
							versuch = versuch +1;
						}
					}
				} while (versuch == 0);
				versuch = 0;
				// System.out.println(
				// kinddaten[0] + " " + kinddaten[1] + " " + kinddaten[2] + " " + kinddaten[3] +
				// " " + wohnort);
			}
		} catch (Exception e) {
			System.out.println("Nicht Vorhanden!!!");
			e.printStackTrace();
		}
		return this.kinder;
	}
}