package Fibo;

public class fibo {

	public static void main(String[] args) {
		System.out.println(fiboIterativer(8));
	}

	public static int fiboRekusiv(int n) {
		int e = 0;
		int j = 1;

		if (n == 0) {
			return e;
		}

		return n;
	}

	public static int fiboIterativer(int n) {
		int e = 0;
		int j = 1;
		int p;

		if (n == 0) {
			return e;
		}
		for (int i = n; i > 0; i--) {
			p = e;
			e = e + j;
			j = p;
		}
		return e;
	}
}