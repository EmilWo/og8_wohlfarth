package suchalgorithmen;

public class LineareSuche implements ISuchalgorithmus {

  @Override
  public int suche(long[] zahlen, long gesuchteZahl) {
    for (int i = 0; i < zahlen.length; i++)
      if (zahlen[i] == gesuchteZahl)
        return i;
    return NICHT_GEFUNDEN;
  }

  @Override
  public int getVersuche(long[] zahlen, long gesuchteZahl) {
    int vergleiche = 0;
    for (int i = 0; i < zahlen.length; i++) {
      vergleiche++;
      if (zahlen[i] == gesuchteZahl)
        return vergleiche;
    }
    return vergleiche;
  }

}