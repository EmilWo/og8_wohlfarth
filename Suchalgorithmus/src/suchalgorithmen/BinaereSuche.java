package suchalgorithmen;

public class BinaereSuche implements ISuchalgorithmus {

  @Override
  public int suche(long[] zahlen, long gesuchteZahl) {
    int l = 0, r = zahlen.length - 1;
    int m;

    while (l <= r) {
      // Bereich halbieren
      m = l + ((r - l) / 2);

      if (zahlen[m] == gesuchteZahl) // Element gefunden?
        return m;

      if (zahlen[m] > gesuchteZahl)
        r = m - 1; // im linken Abschnitt weitersuchen
      else
        l = m + 1; // im rechten Abschnitt weitersuchen
    }
    return NICHT_GEFUNDEN;
  }

  @Override
  public int getVersuche(long[] zahlen, long gesuchteZahl) {
    int l = 0, r = zahlen.length - 1;
    int m;
    int vergleiche = 0;

    while (l <= r) {
      // Bereich halbieren
      m = l + ((r - l) / 2);

      vergleiche++;
      if (zahlen[m] == gesuchteZahl) // Element gefunden?
        return vergleiche;

      vergleiche++;
      if (zahlen[m] > gesuchteZahl) {
        r = m - 1; // im linken Abschnitt weitersuchen
      } else {
        l = m + 1; // im rechten Abschnitt weitersuchen
      }
    }
    return vergleiche;
  }
}