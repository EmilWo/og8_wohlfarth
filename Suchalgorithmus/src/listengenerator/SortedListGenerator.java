package listengenerator;

import java.util.Random;
import suchalgorithmen.*;

public class SortedListGenerator {

	public static long[] getSortedList(int laenge) {
		Random rand = new Random(111111);
		long[] zahlenliste = new long[laenge];
		long naechsteZahl = 0;

		for (int i = 0; i < laenge; i++) {
			naechsteZahl += rand.nextInt(3) + 1;
			zahlenliste[i] = naechsteZahl;
		}

		return zahlenliste;
	}

	public static void main(String[] args) {
		final int ANZAHL = 20_000_000; // 20.000.000

		long[] a = getSortedList(ANZAHL);
		long gesuchteZahl = a[1000000];
		System.out.println(gesuchteZahl);

		long time = System.currentTimeMillis();

		LineareSuche such = new LineareSuche();
		System.out.println(such.suche(a, gesuchteZahl));

		System.out.println(System.currentTimeMillis() - time + "ms");

		// for(long x: a)
		// System.out.println(x);
		long times = System.currentTimeMillis();

		BinaereSuche search = new BinaereSuche();
		System.out.println(search.suche(a, gesuchteZahl));

		System.out.println(System.currentTimeMillis() - times + "ms");
	}
}