package rekursion;

public class MiniMath {

	/**
	 * Berechnet die Fakult�t einer Zahl (n!)
	 * 
	 * @param n
	 *            - Angabe der Zahl
	 * @return n!
	 */
	public static long berechneFakultaet(long n) {
		if (n == 0) {
			return 1;
		} else {
			return n * berechneFakultaet(n - 1);
		}
	}

	/**
	 * Berechnet die 2er-Potenz einer gegebenen Zahl
	 * 
	 * @param n
	 *            - Angabe der Potenz (max. 31 sonst Integeroverflow)
	 * @return 2^n
	 */
	public static long berechneZweiHoch(long n) {
		if (n == 0) {
			return 1;
		} else {
			return 2 * berechneZweiHoch(n - 1);
		}
	}

	/**
	 * Die Methode berechnet die Summe der Zahlen von 1 bis n (also 1+2+...+(n-1)+n)
	 * 
	 * @param n
	 *            - ober Grenze der Aufsummierung
	 * @return Summe der Zahlen 1+2+...+(n-1)+n
	 */
	public static int berechneSumme(int n) {
		if (n == 0) {
			return 0;
		} else {
			return n + berechneSumme(n - 1);
		}
	}

}
