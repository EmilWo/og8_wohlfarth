package omnom;

public class Haustier {
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	private String name;

	public Haustier() {
		super();
	}

	public Haustier(String name) {
		super();
		this.name = name;
		this.gesund = 100;
		this.hunger = 100;
		this.muede = 100;
		this.zufrieden = 100;
	}

	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		if (hunger > 100) {
			this.hunger = 100;
		} else if (hunger < 0) {
			this.hunger = 0;
		} else {
			this.hunger = hunger;
		}
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {
		if (muede > 100) {
			this.muede = 100;
		} else if (muede < 0) {
			this.muede = 0;
		} else {
			this.muede = muede;
		}
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		if (zufrieden > 100) {
			this.zufrieden = 100;
		} else if (zufrieden < 0) {
			this.zufrieden = 0;
		} else {
			this.zufrieden = zufrieden;
		}
	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {
		if (gesund > 100) {
			this.gesund = 100;
		} else if (gesund < 0) {
			this.gesund = 0;
		} else {
			this.gesund = gesund;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void fuettern(int futter) {
		this.setHunger(futter + this.hunger);
	}

	public void schlafen(int schlaf) {
		this.setMuede(schlaf + this.muede);
	}

	public void spielen(int spiel) {
		this.setZufrieden(spiel + this.zufrieden);
	}

	public void heilen() {
		this.setGesund(100);
	}
}