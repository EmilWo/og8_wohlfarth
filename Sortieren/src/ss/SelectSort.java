package ss;

public class SelectSort {
	int versuch = 0;

	public int [] selectsort(int [] a, int l, int r) {
		int q;  
		if (l < r)  {
		    q = max(a, l, l+1, r);
		    wechsel(a, q, r);
		    selectsort(a, l, r-1);
		  }
		return a;
		}

		public int max(int[] a, int q, int l, int r) {
		  if (l <= r) {
		    if (a[l] > a[q]) {
		    	inkrementVertauschungen();
		      return max(a, l, l+1, r);
		    } else {
		    	inkrementVertauschungen();
		      return max(a, q, l+1, r);
		    }
		  } else {
		    return q;
		  }
		}

	private void wechsel(int[] a, int l, int r) {
		int b = 0;
		b = a[l];
		a[l] = a[r];
		a[r] = b;
	}

	private void inkrementVertauschungen() {
		versuch++;
	}

	public long getVertauschungen() {
		return versuch;
	}

	public static String array2str(int[] a) {
		String result = "";

		for (int i = 0; i < a.length; i++) {

			result = result + a[i] + "  ";

		}
		return result;
	}
}