package bs;

public class Bubblesort {

	private long versuche = 0;

	public void bubblesort(long[] a) {

		long b;

		for (int i = a.length - 1; i >= 1; i--) {
			for (int j = 0; j < i; j++) {
				if (a[j] > a[j + 1]) {
					b = a[j];
					a[j] = a[j + 1];
					a[j + 1] = b;
					inkrementVertauschungen();
				} else {
					inkrementVertauschungen();
				}
			}
			System.out.println(array2str(a));
		}

	}

	private void inkrementVertauschungen() {
		versuche++;
	}

	public long getVertauschungen() {
		return this.versuche;
	}

	public static String array2str(long[] a) {
		String result = "";

		for (int i = 0; i < a.length; i++) {

			result = result + a[i] + "  ";

		}
		return result;
	}
}