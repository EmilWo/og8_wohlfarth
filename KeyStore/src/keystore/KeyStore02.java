package keystore;

import java.util.*;

public class KeyStore02 {
	private ArrayList <String>keystore;

	public KeyStore02() {
		this.keystore = new ArrayList<String>(100);
	}

	public KeyStore02(int length) {
		this.keystore = new ArrayList<String>(length);
	}

	public ArrayList<String> getKeystore() {
		return keystore;
	}

	public void setKeystore(ArrayList<String> keystore) {
		this.keystore = keystore;
	}

	public boolean add(String eintrag) {
		this.keystore.add(eintrag);
		return true;
	}

	public int indexOf(String eintrag) {
		return this.keystore.indexOf(eintrag);
	}

	public void remove(int index) {
		this.keystore.remove(index);
	}


	public String toString() {
		String s = "";
		for (int i = 0; i < this.keystore.size(); i++) {
			if (this.keystore != null) {
				s += this.keystore.get(i) + "\n";
			}
		}
		return s;
	}
}