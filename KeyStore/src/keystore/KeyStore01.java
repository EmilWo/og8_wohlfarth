package keystore;

public class KeyStore01 {

	private String[] keystore;

	public KeyStore01() {
		this.keystore = new String[100];
	}

	public KeyStore01(int length) {
		this.keystore = new String[length];
	}

	public boolean add(String eintrag) {
		for (int i = 0; i < this.keystore.length; i++) {
			if (this.keystore[i] == null) {
				this.keystore[i] = eintrag;
				return true;
			}
		}
		return false;
	}

	public int indexOf(String eintrag) {
		for (int i = 0; i < this.keystore.length; i++) {
			if (this.keystore[i].equals(eintrag)) {
				return i;
			}
		}
		return -1;
	}

	public void remove(int index) {
		this.keystore[index] = null;
		for (int i = index; i < this.keystore.length - 1; i++) {
			this.keystore[i] = this.keystore[i + 1];
		}
		this.keystore[this.keystore.length - 1] = null;
	}

	public String toString() {
		String s = "";
		for (int i = 0; i < this.keystore.length; i++) {
			if (this.keystore != null) {
				s += this.keystore[i] + "\n";
			}
		}
		return s;
	}
}