package prim;

import java.util.Scanner;

public class PrimzahlTest {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Primzahltester pri = new Primzahltester();
		Stopuhr st = new Stopuhr();
		long a;
		String b;
		do {
			System.out.print("Geben Sie eine Zahl ein: ");
			a = scan.nextLong();
			st.start();
			System.out.println(pri.isPrimzahl(a));
			st.stop();
			System.out.println(st.getDauerInMs());
			System.out.println("Noch eine Primzahl?");
			b = scan.next();
		} while (b.equals("Ja"));
	}

}
