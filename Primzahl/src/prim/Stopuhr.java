package prim;

public class Stopuhr {
	private long startzeit;
	private long endzeit;

	public long getStartzeit() {
		return startzeit;
	}

	public void setStartzeit(long startzeit) {
		this.startzeit = startzeit;
	}

	public long getEndzeit() {
		return endzeit;
	}

	public void setEndzeit(long endzeit) {
		this.endzeit = endzeit;
	}

	public void start() {
		this.startzeit = System.currentTimeMillis();
	}

	public void stop() {
		this.endzeit = System.currentTimeMillis();
	}

	public void reset() {
		this.endzeit = 0;
		this.startzeit = 0;
	}

	public long getDauerInMs() {
		long m = this.endzeit - this.startzeit;
		return m;
	}

}